<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FulfillmentBatch extends Model
{
    protected $table = 'fulfillment_batches';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
    ];

    /*
    status
    0 - draft
    1 - pending
    2 - completed
    */
    public function orders() {
        return $this->hasMany(Order::class,'batch_id','id')->orderBy('created_at','desc');
    }
}
