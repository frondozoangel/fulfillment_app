<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Auth;
use App\FulfillmentBatch;

class ArchiveController extends Controller
{
    private $url_prefix;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->url_prefix = 'https://'.env('SHOPIFY_KEY').':'.env('SHOPIFY_PASSWORD').'@pfa-dev.myshopify.com/admin/api/2019-10';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = Auth::user();
        $batches = FulfillmentBatch::orderBy('created_at','desc')->paginate(25);
        return view('archives', [
            "user" => $user,
            "batches" => $batches
        ]);
    }

    public function show($id)
    {
        $user = Auth::user();
        $batch = FulfillmentBatch::find($id);
        $orders = $batch->orders()->get();
        $orders_count = count($orders);
        $fulfilled_count = $batch->orders()->where('is_fulfilled',1)->count();
        $error_count = $batch->orders()->where('is_fulfilled',3)->count();
        return view('archive_single', [
            "user" => $user,
            "batch" => $batch,
            "orders" => $orders,
            "orders_count" => $orders_count,
            "fulfilled_count" => $fulfilled_count,
            "error_count" => $error_count,
        ]);   
    }

    public function products(){
        $url = $this->url_prefix.'/products.json';
        $products = $this->shopifyRequest('GET',$url,array());
        return $products;
    }

    public function shopifyRequest($method,$url,$params = array()){
        $client = new Client();
        $res = $client->request($method,$url,$params);
        $body = $res->getBody();
        $body = (string) $body;
        $body = json_decode($body, true);
        $head = $res->getHeader('Link');
        $result = array("body" => $body, "header" => $head);
        //echo "<pre>".print_r(compact('result'),true)."</pre>"; die();
        return($result);
    }
}
