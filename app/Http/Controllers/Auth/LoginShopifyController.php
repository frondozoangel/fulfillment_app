<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserProviders;
use App\Store;
use App\Setting;
use App\Http\Controllers\Controller;
use Socialite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginShopifyController extends Controller
{

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {

        $this->validate($request, [
            'shop' => 'string|required'
        ]);

        $config = new \SocialiteProviders\Manager\Config(
            env('SHOPIFY_KEY'),
            env('SHOPIFY_SECRET'),
            env('SHOPIFY_REDIRECT'),
            ['subdomain' => str_replace(".myshopify.com","",$request->get('shop'))]
        );

        return Socialite::with('shopify')
            ->setConfig($config)
            ->scopes(['read_content', 'write_content', 'read_products', 'write_products', 'read_orders', 'write_orders', 'read_fulfillments', 'write_fulfillments', 'read_shipping', 'write_shipping', 'read_checkouts', 'write_checkouts'])
            ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $shopifyUser = Socialite::driver('shopify')->user();

        // Create user
        $user = User::firstOrCreate([
            'name' => $shopifyUser->nickname,
            'email' => $shopifyUser->email,
            'password' => '',
        ]);

        UserProviders::firstOrCreate([
            'user_id' => $user->id,
            'provider' => 'shopify',
            'provider_user_id' => $shopifyUser->id,
            'provider_token' => $shopifyUser->token,
        ]);

        $shop = Store::firstOrCreate([
            'name' => $shopifyUser->name,
            'domain' => $shopifyUser->nickname,
        ]);

        $shop->users()->syncWithoutDetaching([$user->id]);

        Auth::login($user, true);

        return redirect('/home');
    }
}