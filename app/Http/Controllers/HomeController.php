<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Client;
use App\FulfillmentBatch;
use App\Order;
use Auth;
use DB;

class HomeController extends Controller
{
    private $url_prefix;
    private $setting;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->setting = DB::table('settings')->where('id',1)->first();
        //$this->url_prefix = 'https://'.decrypt($this->setting->api_key).':'.decrypt($this->setting->api_password).'@pfa-dev.myshopify.com/admin/api/2019-10';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        //$user = Auth::user();
        $batch = $this->getOrCreateBatch();
        $orders = $batch->orders()->get();
        return view('home', [
            //"user" => $user,
            "batch" => $batch,
            "orders" => $orders
        ]);
    }

    public function getOrCreateBatch(){
        $batch = FulfillmentBatch::where('status',0)->orWhere('status',1)->orderBy('created_at', 'desc')->first();
        if (empty($batch)) {
            $batch = new FulfillmentBatch;
            $batch->status = 0;
            $batch->save();
        } else {
            $orders_count = $batch->orders()->count();
            if ($orders_count == 0) {
                $batch->created_at = date('Y-m-d h:i:s');
                $batch->save();
            }
        }
        return $batch;
    }

    public function fulfill($batch_id,$order_id){
        $batch = FulfillmentBatch::find($batch_id);
        $batch->status = 1;
        $batch->save();
        $settings = DB::table('settings')->where('id',1)->first();
        $order = Order::find($order_id);
        if ($order->is_fulfilled == 0) {
            $tracking_url = $settings->base_tracking_url.'?code='.$order->tracking_number;
            $url = $this->url_prefix . '/orders/'.$order->order_id.'/fulfillments.json';
            $json = '{
                "fulfillment": {
                    "location_id": '.$settings->store_location_id.',
                    "tracking_number": "'.$order->tracking_number.'",
                    "tracking_url": "'.$tracking_url.'",
                    "notify_customer": true
                }
            }';
            $headers = array(
                "Content-Type" => "application/json"
            );
            $result = $this->shopifyRequest('POST',$url,[
                "headers" => $headers,
                "body" => $json,
            ]);
            $r = json_decode($result,true);
            if (isset($r['fulfillment'])) {
                $order->fulfillment_id = $r['fulfillment']['id'];
                $order->is_fulfilled = 1;
                $order->fulfilled_at = date('Y-m-d h:i:s');
                $order->save();
            }
            else {
                $order->error_log = $result;
                $order->is_fulfilled = 3;
                $order->save();
            }
        }
        return response()->json([
            "status" => "done",
            "batch_id" => $batch_id,
            "order" => $order
        ]);
    }

    public function complete($id)
    {
        $batch = FulfillmentBatch::find($id);
        $batch->status = 2;
        $o_success = $batch->orders()->where('is_fulfilled',1)->count();
        $o_total = $batch->orders()->count();
        $o_errors = $batch->orders()->where('is_fulfilled',3)->count();
        if ($batch->save()) {
            return response()->json([
                "status" => "success",
                "batch_id" => $id,
                "details" => 'COMPLETE! '.$o_success.' of '.$o_total.' orders fulfilled. '.$o_errors.' Errors. <a href="'.route('show.archive', $id).'">Show batch archived</a> or <a href="/home">start a new batch.</a>'
            ]);
        } else {
            return response()->json([
                "status" => "failed",
                "batch_id" => $id,
            ]);
        }

    }

    /* FOR TESTING PURPOSE */
    public function getLocation()
    {
        $url = $this->url_prefix . '/locations.json';
        $headers = array(
            "Content-Type" => "application/json"
        );
        $result = $this->shopifyRequest('GET',$url,["headers"=>$headers]);
        print_r($result);
    }

    public function getOrderFulfillment()
    {
        $url = $this->url_prefix . '/orders/1920090570892/fulfillments.json';
        $headers = array(
            "Content-Type" => "application/json"
        );
        $result = $this->shopifyRequest('GET',$url,["headers"=>$headers]);
        print_r($result);
    }

    public function shopifyRequest($method,$url,$params = array()){
        $client = new Client();
        $res;
        try {
            $res = $client->request($method,$url,$params)->getBody();
        } catch (\Exception $e) {
            $res = $e->getMessage();
        }
        
        $body = $res;
        $body = (string) $body;
        return($body);
    }
}
