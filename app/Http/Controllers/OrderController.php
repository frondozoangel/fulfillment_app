<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\FulfillmentBatch;
use App\Order;

class OrderController extends Controller
{
	public function create($batch_id,Request $request)
	{
		$data = $request->except('_token');
		$data['batch_id'] = $batch_id;

		$order = new Order;
		$order->order_id = $data['order_id'];
		$order->batch_id = $batch_id;
		$order->tracking_number = $data['tracking_number'];
		$order->is_fulfilled = 0;
		$order->save();

		if ($order->wasRecentlyCreated) {
			return response()->json([
				"status" => "success",
				"order" => $order
			]);
		} else {
			return response()->json([
				"status" => "error",
				"message" => "Something went wrong. Please contact the administrator"
			]);
		}
	}

	public function get($id,Request $request)
	{
		$order = Order::find($id);
		if ($request->ajax()) {
			return response()->json(["order"=>$order]);
		}
		return json_encode($order);
	}

	public function delete($id)
	{
		$order = Order::destroy($id);
		return response()->json(['status'=>'success']);
	}
}