<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Auth;
use DB;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = Auth::user();
        $settings = DB::table('settings')->where('id',1)->first();
        return view('settings', [
            "user" => $user,
            "settings" => $settings
        ]);
    }

    public function save(Request $request)
    {
        $data = array(
            'store_location_id' => $request->input('store_location_id'),
            'base_tracking_url' => $request->input('base_tracking_url'),
            'api_key' => encrypt($request->input('api_key')),
            'api_password' => encrypt($request->input('api_password')),
        );
        $settings = DB::table('settings')->where('id',1)->update($data);
        return redirect(route('settings'))->with('status','Successfully udpated!');
    }

}
