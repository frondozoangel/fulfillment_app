<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'batch_id', 'order_id', 'tracking_number', 'is_fulfilled', 'fulfilled_at', 'error_log', 'fulfillment_id'
    ];

    /*
	status
	0 - X (hasn't start batch fulfill)
	1 - Y
	2 - Pending
	3 - Error
    */

    public function fulfillmentBatch(){
    	return $this->belongsTo(FulfillmentBatch::class,'id','batch_id');
    }
}
