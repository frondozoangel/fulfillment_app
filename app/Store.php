<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'domain',
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    public function user()
    {
        return $this->belongsToMany(
            'App\User', 'store_users', 'store_id', 'user_id'
        );
    }
}
