$(function(){
	function a(b){
		var c = $('#txt-order_id'); e(c,b.order_id);
		var d = $('#txt-tracking_number'); e(d,b.tracking_number);
		var h = $('#txt-error_msg'); e(h,b.error_log);
	}
	function e(f,g){
		f.empty();
		f.text(g);
	}
	$('.error-link').click(function(){
		var i = $(this).data('id');
		var t = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			url: '/orders/'+i,
			method: 'GET',
			data: {'_token':t},
			success: function(response){
				var o = response.order;
				if (typeof o !== 'undefined') {
					a(o);
				}
			}
		});
	})
});