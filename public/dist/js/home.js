$(function(){
	var a = $('#form-detail-entry #order_id');
	var b = $('#form-detail-entry #tracking_number');
	var loader = '<div class="loader">Loading...</div>';
	$('#btn-fulfill_batch').click(function(){
		an();
		var tbd = $('#tbl-fbatch tbody');
		var f = $('#form-fulfill-batch');
		var ac = f.attr('action');
		if (tbd.children().length > 0) {
			var tr = $('#tbl-fbatch tbody tr');
			var ctr = tr.length;
			tr.each(function(){
				var a = $(this);
				var id = $(this).data('id');
				var url = ac.replace('id', id);
				$.ajax({
					url: url,
					method: f.attr("method"),
					data: f.serialize(),
					beforeSend: function(){
					},
					success: function(response) {
						if (response.status == 'done') {
							if (response.order.is_fulfilled == 1) {
								a.closest('.status').html('Y');
							} else {
								a.closest('.status').html('ERROR!');
							}
						}
						else {
							Swal.fire(
								'Warning',
								'Something went wrong.',
								'warning'
							);
						}
					}
				});
			});
			var sc = (ctr * 1800);
			setTimeout(k, 3000);
		} else {
			Swal.fire(
				'Warning',
				'Fill in the input fields.',
				'warning'
			);
		}
	});
	ri();

    // this fires everytime text is entered on the order id box.
	a.on({
	    textInput: function() {
	    	timer($(this));
	    }
	});

    // this fires everytime text is entered on the tracking number box.
	b.on({
	    textInput: function() {
            timer($(this));
	    }
	});

    // timer should set a delay before proceeding to the next action.
    var timerstart = false;
    var si;
    function timer(e){
	    var now = e.val();
        if(timerstart == false){
            console.log('now: '+now);   
            timerstart = true;
        	si = setInterval(function(){
        	    var after = e.val();
        	    console.log('after delay: '+after);
        		if (e.val().length > 0 && now == after) {
        		    clearInterval(si);
                    timerstart = false;
        			switch(e.data('sequence')){
        				case 1:
        					b.focus();
        				break;

        				case 2:
        					c();
        				break;
        			}
        		} else {
        		  now = after;
                  console.log('now updated: '+now);
        		}
        	}, 1000);
        }
	}

    // this checks if a and b are both filled before attempting to write the order via ajax.
	function c(){
		if (a.val().length > 0 && b.val().length > 0) {
			$('#btn-fulfill_batch').prop("disabled", true);
			var f = $('#form-detail-entry');
			$.ajax({
				url: f.attr("action"),
				method: f.attr("method"),
				data: f.serialize(),
				success: function(response) {
					if (response.status == 'success') {
						d(response.order);
						j();
					}
					else {
						Swal.fire(
							'Warning',
							response.message,
							'warning'
						);
					}
				}
			}).done(function(){
				$('#btn-fulfill_batch').prop("disabled", false);
			});
		}
	}

    // this displays the saved record on the front end.
	function d(data){
		var e = $('#tbl-fbatch');
		var g = null;
		if(data.is_fulfilled == 0){g='X';}else{g='Y';}
		var h = '<tr data-id="'+data.id+'"><td>'+data.order_id+'</td><td>'+data.tracking_number+'</td><td class="text-center">'+g+'</td><td><a href="javascript:void(0)" class="btn-remove text-danger" data-id="'+data.id+'"><i class="far fa-trash-alt"></i></a></td></tr>';
		e.find('tbody').prepend(h);
		ri();
	}
	function j(){
		a.val('');
		b.val('');
		a.focus();
	}
	function k(){
		var dc = $('#details-container');
		var f = $('#form-complete-batch');
		var u = f.attr('action');
		$.ajax({
			url: u,
			method: f.attr('method'),
			data: f.serialize(),
			success: function(response) {
				if (response.status == 'success') {
				$('.loading').fadeOut("slow");
					dc.empty();
					dc.append(response.details);
				} else {
					Swal.fire(
						'Warning',
						'Something went wrong. Please contact the administrator.',
						'error'
					);
				}
			}
		})
	}
	function an(){
		$('.loading').fadeIn("slow");
	}
	function ri(){
		$('.btn-remove').click(function(){
			var p = $(this).parent().parent();
			var i = $(this).data('id');
			var f = $('#form-remove-item');
			$.ajax({
				url: '/orders/'+i+'/delete',
				method: f.attr('method'),
				data: f.serialize(),
				success: function(response) {
					if (response.status == 'success') {
						p.remove();
					}
				}
			});
		});
	}
});