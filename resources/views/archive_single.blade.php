@extends('layouts.theme')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
@endsection

@section('content-header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12 col-md-8 mx-auto">
        <div class="back-button">
          <a href="{{ route('archives') }}"><i class="fas fa-chevron-left mr-2"></i>Back to archives</a>  
        </div>
      </div>
    </div>
    <div class="row mb-2">
      <div class="col-sm-12 col-md-8 mx-auto">
        <h1 class="m-0 text-dark">Archived Batch #{{ $batch->id }}</h1>
      </div>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
      <div class="col-sm-12 col-md-8 mx-auto">

          <div class="archive-details row">
            <div class="col-md-3">
              <div class="section-block">
                <div class="section-block-label">Date</div>
                <div class="section-block-data">{{ date('d/m/Y', strtotime($batch->created_at)) }}</div>
              </div>
              <div class="section-block">
                <div class="section-block-label">Batch count</div>
                <div class="section-block-data">{{ $orders_count }}</div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="section-block">
                <div class="section-block-label">Result</div>
                <div class="section-block-data">{{ $fulfilled_count }}/{{ $orders_count }} fulfilled</div>
              </div>
              <div class="section-block">
                <div class="section-block-label">Errors</div>
                <div class="section-block-data @if($error_count > 0) text-danger @endif">{{ $error_count }}</div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-border">
                  <thead class="thead-light">
                    <tr>
                      <th>Order ID</th>
                      <th>Tracking Number</th>
                      <th class="text-center">Fulfilled?</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(empty($orders))
                      <tr><td>No data to show.</td></tr>
                    @else
                      @foreach($orders as $order)
                      <tr>
                        <td>{{$order->order_id}}</td>
                        <td>{{$order->tracking_number}}</td>
                        @if($order->is_fulfilled == 0)
                        <td class="text-center">X</td>
                        @elseif($order->is_fulfilled == 1)
                        <td class="text-center">Y</td>
                        @elseif($order->is_fulfilled == 2)
                        <td class="text-center">Pending</td>
                        @else
                        <td class="text-center"><a href="#" onclick="return false;" class="error-link" data-id="{{$order->id}}">ERROR!</a></td>
                        @endif
                      </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
              </div>
            </div>
            @if($batch->orders()->where('is_fulfilled',3)->count() > 0)
            <div class="error-log row">
              <div class="col-md-12">
                <div class="col-label"><h3>Error Log</h3></div>
                <div class="order-detail">
                  <div class="order-detail-label">Order ID</div>
                  <div id="txt-order_id"></div>
                </div>
                <div class="order-detail">
                  <div class="order-detail-label">Tracking Number</div>
                  <div id="txt-tracking_number"></div>
                </div>
                <div class="errors-feed">
                  <span id="txt-error_msg" class="font-italic">Error message here</span>
                </div>
              </div>
            </div>
            @endif
      </div>
  </div>
@endsection

@section('js')
<script src="/dist/js/archives.js"></script>
@endsection