@extends('layouts.theme')

@section('content-header')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12 col-md-8 mx-auto">
        <h1 class="m-0 text-dark">Archived Batches</h1>
      </div>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
      <div class="col-sm-12 col-md-8 mx-auto">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-border">
                  <thead class="thead-light">
                    <tr>
                      <th>Date</th>
                      <th class="text-center">Order Count</th>
                      <th>Result</th>
                      <th class="text-center">Errors</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(!empty($batches))
                      @foreach($batches as $batch)
                        <tr>
                          <td>{{ date('M j, Y',strtotime($batch->created_at)) }} @if($batch->status == 0) (draft) @endif</td>
                          <td class="text-center">{{ $batch->orders()->count() }}</td>
                          <td>{{ $batch->orders()->where('is_fulfilled',1)->count() }}/{{ $batch->orders()->count() }} fulfilled</td>
                          <td class="text-center text-danger">{{ $batch->orders()->where('is_fulfilled',3)->count() }}</td>
                          <td class="text-center"><a href="{{ route('show.archive', ['id'=>$batch->id]) }}">View Batch</a></td>
                        </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
  </div>
@endsection