@extends('layouts.theme')

@section('css')
<link rel="stylesheet" type="text/css" href="/css/styles.css">
@endsection

@section('content')
  <div class="row">
      <div class="col-sm-12 col-md-8 mx-auto">
          <div class="section">
            <form action="{{route('add.order.row', ['batch_id' => $batch->id])}}" method="post" id="form-detail-entry">
              @csrf
              <div class="input-fields">
                  <input type="text" class="form-control" name="order_id" id="order_id" data-sequence="1" placeholder="Order ID" autofocus>
                  <input type="text" class="form-control" name="tracking_number" id="tracking_number" data-sequence="2" placeholder="Tracking Number">
              </div>     
            </form>
            <form action="fulfillment_batch/{{$batch->id}}/fulfill/id" method="post" id="form-fulfill-batch">
              @csrf
              <div class="input-actions">
                  <button type="button" class="btn btn-default" id="btn-fulfill_batch">Fulfill Batch</button>
              </div>  
            </form>
          </div>
          <div class="card">
            <div class="card-body" id="details-container">
              <div class="table-responsive">
                <table class="table table-border table-hover" id="tbl-fbatch">
                  <thead class="thead-light">
                    <tr>
                      <th>Order ID</th>
                      <th>Tracking Number</th>
                      <th class="text-center">Fulfilled?</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(!empty($orders))
                      @foreach($orders as $order)
                      <tr data-id="{{$order->id}}">
                        <td class="order-id">{{$order->order_id}}</td>
                        <td>{{$order->tracking_number}}</td>
                        @if($order->is_fulfilled == 0)
                        <td class="status text-center">X</td>
                        @elseif($order->is_fulfilled == 1)
                        <td class="status text-center">Y</td>
                        @elseif($order->is_fulfilled == 2)
                        <td class="status text-center">Pending</td>
                        @else
                        <td class="status text-center text-danger">ERROR</td>
                        @endif
                        <td>
                          <a href="javascript:void(0)" class="btn-remove text-danger" data-id="{{$order->id}}"><i class="far fa-trash-alt"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            <form action="{{ route('complete.batch', [$batch->id]) }}" method="PUT" id="form-complete-batch">
              @csrf
            </form>
            <form method="POST" id="form-remove-item">
              @csrf
            </form>
            </div>
          </div>
      </div>
  </div>
  <div class="loading">
    <div class='uil-ring-css' style='transform:scale(0.79);'>
      <div></div>
    </div>
  </div>
@endsection

@section('js')
<script src="dist/js/home.js"></script>
@endsection