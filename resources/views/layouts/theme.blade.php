<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Private Fulfillment App</title>

  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <script src="https://kit.fontawesome.com/629baf7da2.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="/dist/css/adminlte.min.css">
  <link rel="stylesheet" type="text/css" href="/dist/css/sweetalert2.min.css">
  @yield('css')
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  @include('partials.sidebar')

  <div class="content-wrapper">
    @yield('content-header')

    <div class="content">
      <div class="container-fluid">
        @yield('content')
      </div>
    </div>

  </div>

  <aside class="control-sidebar control-sidebar-dark">

  </aside>

  <footer class="main-footer">
    <strong>Copyright &copy; 2019.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>
</div>

<script src="/plugins/jquery/jquery.min.js"></script>

<script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/dist/js/adminlte.js"></script>

<script src="/dist/js/demo.js"></script>
<script src="/dist/js/pages/dashboard3.js"></script>

<script src="/dist/js/sweetalert2.min.js"></script>
@yield('js')
</body>
</html>
