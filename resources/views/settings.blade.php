@extends('layouts.theme')

@section('content-header')
<div class="content-header">
  <div class="container-fluid">
    @if (session('status'))
    <div class="row mb-2">
      <div class="col-sm-12 col-md-8 mx-auto">
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      </div>
    </div>
    @endif
    <div class="row mb-2">
      <div class="col-sm-12 col-md-8 mx-auto">
        <h1 class="m-0 text-dark">Settings</h1>
      </div>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
      <div class="col-sm-12 col-md-8 mx-auto">
          <div class="card">
            <div class="card-body">
              <form action="/settings/save" method="post">
                @csrf
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Store Location ID</span>
                  </div>
                  <input type="text" class="form-control" name="store_location_id" id="store_location_id" value="{{ $settings->store_location_id }}" />
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Base Tracking URL</span>
                  </div>
                  <input type="text" class="form-control" name="base_tracking_url" id="base_tracking_url" value="{{ $settings->base_tracking_url }}" />
                </div>
                <div class="mt-4">
                  <div class="form-group">
                    <label for="api_key">Private App API Key</label>
                    <input type="text" class="form-control" id="api_key" name="api_key" placeholder="API Key" value="{{ decrypt($settings->api_key) }}">
                  </div>
                  <div class="form-group">
                    <label for="api_password">Private App Password</label>
                    <input type="password" class="form-control" id="api_password" name="api_password" placeholder="Password" value="{{ decrypt($settings->api_password) }}">
                  </div>
                </div>
                <div class="form-action">
                  <button class="btn btn-secondary btn-block">Save</button>
                </div>
              </form>
            </div>
          </div>
      </div>
  </div>
@endsection