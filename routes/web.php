<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/fulfillment_batch/{batch_id}/orders/add', 'OrderController@create')->name('add.order.row');
Route::post('/fulfillment_batch/{batch_id}/fulfill/{order_id}', 'HomeController@fulfill')->name('fulfill.batch');
Route::put('/fulfillment_batch/complete/{batch_id}', 'HomeController@complete')->name('complete.batch');

Route::get('/archives', 'ArchiveController@index')->name('archives');
Route::get('/archives/{id}', 'ArchiveController@show')->name('show.archive');

Route::get('/orders/{id}', 'OrderController@get');
Route::post('/orders/{id}/delete', 'OrderController@delete')->name('order.delete');

Route::get('/settings', 'SettingController@index')->name('settings');
Route::post('/settings/save', 'SettingController@save')->name('settings.save');

Route::get('login/shopify', 'Auth\LoginShopifyController@redirectToProvider')->name('login.shopify');
Route::get('login/shopify/callback', 'Auth\LoginShopifyController@handleProviderCallback');

//Route::get('login/shopify', 'Auth\LoginShopifyController@redirectToProvider')->name('login.shopify');
//Route::get('login/shopify/callback', 'Auth\LoginShopifyController@handleProviderCallback');